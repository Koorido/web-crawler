# Mail Sender

Ce crawler a été conçu dans le cadre du cours d'Indexation Web de troisième année à l'ENSAI.

## Présentation

Il s'agit d'un crawler simple parcourant les pages à partir de `https://ensai.fr`, dans la limite de 5 liens par page et 50 au total.

## Installation
Pour installer l'apllication, cloner ce dépôt : 

```
git clone https://gitlab.com/Koorido/web-crawler.git
cd web-crawler
```

Enfin exécuter `main.py`
