from lib.crawler import Crawler

#Pour changer les paramètres du crawler
PAGES_MAX = 25
MAX_PAR_PAGE = 5

def main(url):
    c = Crawler(url, pages_max=PAGES_MAX, max_par_page=MAX_PAR_PAGE)
    return c.run()


if __name__ == "__main__":
    main("https://ensai.fr")