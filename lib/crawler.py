from time import sleep
from lib.webpage import WebPage


class Crawler:
    """Web Crawler

    Parcours le web a partir d'une URL donnée, jusqu'a ce que le nombre de pages souhaité soit atteint. Il n'explore qu'une 
    partie des liens référencés sur chaque page.

    """

    def __init__(self, seed, pages_max=50, max_par_page=5):
        """Constructeur
        
        Parameters
        ----------
        seed: str
            URL de la page servant de seed (le crawl doit y être autorisé)
        pages_max: int
            Le nombre de pages souhaité, par défaut 50
        max_par_page: int
            La limite de line a explorer par page, par défaut 5
        """
        self.to_visit = [seed]
        self.discovered = []
        self.max_par_page = max_par_page
        self.pages_max = pages_max
        self.robots = {}


    def run(self):
        progression = 0
        print("Début du crawl\n")
        while self.to_visit and len(self.discovered) < self.pages_max:

            #On extrait de la frontiere la prochaine page a visiter
            target_page = WebPage(self.to_visit.pop(0))
            #On l'ajoute à la liste des pages découvertes
            self.discovered.append(str(target_page))
            #On ajoute à la frontière les nouvelles adresses contenues dans la page (en prenant en compte la limite fixée)
            
            new_url, robots = target_page.get_urls(self.discovered+self.to_visit, self.max_par_page, self.robots)[:self.max_par_page]
            self.to_visit += new_url
            self.robots = robots

            if len(self.discovered)/self.pages_max > progression/self.pages_max:
                progression+=1
                print('Progression: ['+progression*'#'+(50-progression)*'-'+']')
            sleep(3)

            if len(self.to_visit)==0:
                print("Interruption: plus de pages à visiter (frontière vide)\n")
        self.save_results()
        print('Fini !')
    
    def save_results(self):

        with(open('crawled-webpages.txt', 'w')) as f:
            for l in self.discovered:
                f.write(l+'\n')
            f.close()

