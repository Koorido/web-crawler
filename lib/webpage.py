from lib.utils import *
from bs4 import BeautifulSoup
from urllib.robotparser import RobotFileParser
import requests
import validators


class WebPage:
    def __init__(self, url):
        self.url = url
        self.robots = {}
        # self.robots = RobotFileParser()    
    
    def check_link(self, url):
        if not (robots_txt(url) in self.robots.keys()):
            self.robots[robots_txt(url)] = RobotFileParser(robots_txt(url))
            try:
                self.robots[robots_txt(url)].read()
            except: 
                return False
            print("Lecture et enregistrment: {}".format(robots_txt(url)))
        return self.robots[robots_txt(url)].can_fetch("*", url)
        

        # self.robots.set_url(robots_txt(url))
        # self.robots.read()
        # return self.robots.can_fetch('*', url)

    def get_urls(self, known, max_par_page, robots):
        #On passe le dictionnaire des pages robots.txt connues en argument pour les conserver dans le crawler et les transmettre aux
        # prochaines pages et donc ne pas avoir a les retelecharger a chaque fois (ca marche dans le cadre de cete exercice pcq on 
        #crawle sur une courte durée, mais en vrai il faudrait les reactualiser de temps en temps pour tenir compte d'eventuelles evolutions)
        self.robots = robots
        r = requests.get(self.url, headers={'User-Agent':"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:121.0) Gecko/20100101 Firefox/121.0"})
        soup = BeautifulSoup(r.text, 'html.parser')
        hrefs = [lk for lk in list(set(map(lambda l: l.get('href'), soup.find_all('a')))) if validators.url(lk)]
        # shuffle(hrefs)
        out = []
        while hrefs and len(out)<max_par_page:
            link = hrefs.pop(0)
            if not (link in known) and self.check_link(link):
                out.append(link)
        return out, self.robots



        # for link in hrefs:
        #     if 
        # return [lk for lk in hrefs if (not (lk in known) and self.check_link(lk))]

    def __str__(self):
        return self.url

        